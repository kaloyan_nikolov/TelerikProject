// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --

//login
Cypress.Commands.add('login', (email,password) => {
    cy.visit('http://localhost:3000/login');
    cy.url().should('include', 'login');
    cy.get('#email').type(email);
    cy.get('#password').type(password);
    cy.get('#loginButton').click()
    cy.url().should('include', 'home')
})

//registration
Cypress.Commands.add('registration', (username,password, email) => {
    cy.visit('http://localhost:3000/login');
    cy.url().should('include', 'login');
    cy.get('#regLink').click()
    cy.url().should('include', 'registration');
    cy.get('#userName').type(username);
    cy.get('#password').type(password);
    cy.get('#email').type(email);
    cy.get('#registration').click()
    cy.url().should('include', 'login')

})


//login and registration
Cypress.Commands.add('regAndLogin', (username,password,email ) => {
    cy.visit('http://localhost:3000/login');
    cy.url().should('include', 'login');
    cy.get('#regLink').click()
    cy.url().should('include', 'registration');
    cy.get('#userName').type(username);
    cy.get('#password').type(password);
    cy.get('#email').type(email);
    cy.get('#registration').click()
    cy.url().should('include', 'login')
    cy.get('#email').type(email);
    cy.get('#password').type(password);
    cy.get('#loginButton').click()
    cy.url().should('include', 'home')
})

//search
Cypress.Commands.add('search', (value) => {
    cy.get('input').type(value);
    cy.get('#searchButton').click()
    cy.wait(2000)
    cy.get('#itemsContainer').find('div')
})

//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })
