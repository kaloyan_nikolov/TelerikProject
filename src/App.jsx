import React, { useState, useEffect } from "react";
import Navbar from "./pages/protected/NavBar";
import HomePage from "./pages/protected/HomePage";
import Login from "./pages/public/Login";
import Register from "./pages/public/Register";
import Account from "./pages/protected/Account";
import { supabase } from "./lib/supabaseClient";
import { useDispatch, useSelector } from "react-redux";
import { setSession } from "./api/logging/sessionSlice";
import { Route, Routes, Navigate } from "react-router-dom";

export function App() {
  const dispatch = useDispatch();
  const sessionAuth = useSelector((state) => state.userReducer);

  const [isLogin, setLogin] = useState(false);
  const [userName, setUserName] = useState("");

  useEffect(() => {
    supabase.auth.getSession().then(({ data: { session } }) => {
      if (session?.user !== undefined) {
        dispatch(setSession(session));
      }
      setUserName(session?.user.user_metadata.username);
    });
  }, []);

  useEffect(() => {
    supabase.auth.getSession().then(({ data: { session } }) => {
      if (session?.user !== undefined) {
        setLogin(true);
        setUserName(session?.user.user_metadata.username);
      } else {
        setLogin(false);
      }
    });
  }, [sessionAuth]);

  return (
    <>
      <Navbar isLoginStatus={isLogin} userName={userName} />

      {isLogin ? (
        <Routes>
          <Route path="/" element={<Navigate to="/home" replace={true} />} />
          <Route path="/home" element={<HomePage />} />
          <Route path="/account" element={<Account />} />
          <Route
            path="/login"
            element={<Navigate to="/home" replace={true} />}
          />
          <Route
            path="/registration"
            element={<Navigate to="/home" replace={true} />}
          />
          <Route path="*" element={<h1>You found page 404 (:</h1>} />
        </Routes>
      ) : (
        <Routes>
          <Route path="/" element={<Navigate to="/login" replace={true} />} />
          <Route
            path="/home"
            element={<Navigate to="/login" replace={true} />}
          />
          <Route path="/login" element={<Login />} />
          <Route path="/registration" element={<Register />} />
          <Route
            path="/user"
            element={<Navigate to="/login" replace={true} />}
          />
          <Route path="*" element={<h1>You found page 404 (:</h1>} />
        </Routes>
      )}
    </>
  );
}
