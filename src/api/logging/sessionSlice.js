import { createSlice } from "@reduxjs/toolkit";

// const initialState = { };

export const sessionSlice = createSlice({
  name: "Auth",
  initialState: {},
  reducers: {
    setSession: (state, action) => {
      state.Auth = { ...state.session, ...action.payload };
    },
    removeSession: (state) => {
      state.Auth = {};
    },
  },
});

export const { setSession, removeSession } = sessionSlice.actions;

export default sessionSlice.reducer;
