import {createApi, fetchBaseQuery} from "@reduxjs/toolkit/query/react";
import {tags} from "../lib/tags";

export const baseApi = createApi({
    reducerPath: "API",
    baseQuery: fetchBaseQuery({
        baseUrl: "https://xpskjwfrmcrixsxccfki.supabase.co",
        prepareHeaders: (headers) => {
            const token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6Inhwc2tqd2ZybWNyaXhzeGNjZmtpIiwicm9sZSI6ImFub24iLCJpYXQiOjE2NzAxMzk4NjksImV4cCI6MTk4NTcxNTg2OX0.zFLGYLl5heWt15WfruiW9xtXz6Kf9LpWQVTtvxTXiT0";
            if (token) headers.set("authorization", `Bearer ${token}`);
            if (token) headers.set("apikey", `${token}`);
            headers.set("Content-type", "application/json");
            return headers;
        },
    }),
    tagTypes: tags,
    endpoints: () => ({}),
});



