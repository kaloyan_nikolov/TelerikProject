import {baseApi} from "../baseApi";

export const filterManufacturerApi = baseApi.injectEndpoints({

    endpoints: (builder) => ({
        getAllManufacturer: builder.query({
            query: () => ({
                url: "/rest/v1/manufacturers",
                method: "GET",
            }),
            providesTags: ["manufacturers"],
        })
    }),
});

export const {useGetAllManufacturerQuery} = filterManufacturerApi;

