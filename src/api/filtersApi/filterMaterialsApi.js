import {baseApi} from "../baseApi";

export const filterMaterialsApi = baseApi.injectEndpoints({

    endpoints: (builder) => ({
        getAllMaterials: builder.query({
            query: () => ({
                url: "/rest/v1/materials",
                method: "GET",
            }),
            providesTags: ["materials"],
        })
    }),
});

export const {useGetAllMaterialsQuery} = filterMaterialsApi;

