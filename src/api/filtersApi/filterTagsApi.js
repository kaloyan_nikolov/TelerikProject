import {baseApi} from "../baseApi";

export const filterTagsApi = baseApi.injectEndpoints({

    endpoints: (builder) => ({
        getAllTags: builder.query({
            query: () => ({
                url: "/rest/v1/tags",
                method: "GET",
            }),
            providesTags: ["tags"],
        })
    }),
});

export const {useGetAllTagsQuery} = filterTagsApi;

