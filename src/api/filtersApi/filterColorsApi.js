import {baseApi} from "../baseApi";

export const filterColorsApi = baseApi.injectEndpoints({

    endpoints: (builder) => ({
        getAllColor: builder.query({
            query: () => ({
                url: "/rest/v1/colors",
                method: "GET",
            }),
            providesTags: ["colors"],
        })
    }),
});

export const {useGetAllColorQuery} = filterColorsApi;

