import {baseApi} from "../baseApi";

export const filterIndustriesApi = baseApi.injectEndpoints({

    endpoints: (builder) => ({
        getAllIndustries: builder.query({
            query: () => ({
                url: "/rest/v1/industries",
                method: "GET",
            }),
            providesTags: ["industries"],
        })
    }),
});

export const {useGetAllIndustriesQuery} = filterIndustriesApi;

