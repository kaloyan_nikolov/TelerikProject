import { configureStore } from "@reduxjs/toolkit";
import { rootMiddleware, rootReducer } from "./rootReducer";

const store = configureStore({

  reducer: rootReducer,
  middleware: rootMiddleware,
});

export default store;
