import {combineReducers} from "redux";
import {baseApi} from "../api/baseApi";
import filterType from "../slices/filterType";
import filterColors from "../slices/filterColors";
import filterManufacturer from "../slices/filterManufacturer";
import filterTags from "../slices/filterTags";
import filterIndustries from "../slices/filterIndustries";
import userReducer from "../api/logging/sessionSlice";


export const rootReducer = combineReducers({
    [baseApi["reducerPath"]]: baseApi["reducer"],
    filterType,
    filterColors,
    filterManufacturer,
    filterTags,
    filterIndustries,
    userReducer,
});

export const rootMiddleware = (getDefaultMiddleware) =>
    getDefaultMiddleware().concat(baseApi["middleware"]);
