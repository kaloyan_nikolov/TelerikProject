import React from "react";
import PageFormBox from "../../components/organisms/layout/PageFormBox";

const Account = () => {
  return (
    <>
      <PageFormBox type="account" />
    </>
  );
};

export default Account;
