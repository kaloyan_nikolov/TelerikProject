import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { supabase } from "../../lib/supabaseClient";
import { useDispatch, useSelector } from "react-redux";
import { removeSession } from "../../api/logging/sessionSlice";
import { Link, redirect } from "react-router-dom";
import styles from "./NavBar.module.css";
import LogoChaos from "../../../public/images/Chaos-Group-logo.svg";

const Navbar = ({ isLoginStatus, userName }) => {
  const dispatch = useDispatch();
  const sessionAuth = useSelector((state) => state.userReducer);

  const [loginStatus, setLoginStatus] = useState(isLoginStatus);

  const logOut = async () => {
    const { error } = await supabase.auth.signOut();

    if (!error) {
      dispatch(removeSession());
      redirect("/login");
    } else {
      alert(error);
    }
  };

  useEffect(() => {
    if (isLoginStatus) {
      setLoginStatus(true);
    } else {
      setLoginStatus(false);
    }
  }, [sessionAuth, isLoginStatus]);

  return (
    <div className={styles.navbar}>
      <img
        src={LogoChaos}
        alt="Chaos Group Logo"
        className={styles.logoNavbar}
      />

      {loginStatus ? (
        <>
          <span style={{ color: "white" }}>Hello, {userName}!</span>
          <Link to="/account">Account</Link>
          <button onClick={() => logOut()}>Log out</button>
        </>
      ) : (
        <>
          <Link to="/login">Login</Link>{" "}
          <Link to="/registration">Registration</Link>
        </>
      )}
    </div>
  );
};

Navbar.propTypes = {
  isLoginStatus: PropTypes.bool.isRequired,
  userName: PropTypes.string,
};

export default Navbar;
