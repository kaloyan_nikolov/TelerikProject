import React from "react";
import PageMain from "../../components/organisms/layout/PageMain";
import PageMedia from "../../components/organisms/layout/PageMedia";
import Carousel from "../../components/molecules/Carousel";
import Aside from "../../components/utilized/home/Aside";
import ItemsContainer from "../../components/organisms/gallery/ItemsContainer";

const HomePage = () => {

  return (
    <>
      <PageMedia>
        <Carousel transitionDuration={1000} />
      </PageMedia>
      <PageMain>
        <Aside />
        <ItemsContainer/>
      </PageMain>
    </>
  );
};

export default HomePage;
