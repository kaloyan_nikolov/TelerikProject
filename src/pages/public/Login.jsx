import React from "react";
import PageFormBox from "../../components/organisms/layout/PageFormBox";

const Login = () => {
  return (
    <PageFormBox type="login"/>
  );
};

export default Login;
