import React from "react";
import PageFormBox from "../../components/organisms/layout/PageFormBox";

const Register = () => {
  return (
    <PageFormBox type="registration"/>
  );
};

export default Register;
