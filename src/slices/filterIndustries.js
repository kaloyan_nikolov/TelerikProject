import {createSlice} from "@reduxjs/toolkit";

export const filterIndustries = createSlice({
    name: "filterIndustries",
    initialState: {id: []},
    reducers: {
        setIndustriesId(state, actions) {
            if (!state.id.includes(actions.payload)) {
                state.id = [...state.id, actions.payload];
            }
        },
        removeIndustriesId(state, actions) {
            state.id = state.id.filter(current => current !== actions.payload);
        },
        resetIndustriesIds(state) {
            state.id = [];
        }
    }
});

export const {setIndustriesId, removeIndustriesId, resetIndustriesIds} = filterIndustries.actions;

export default filterIndustries.reducer;
