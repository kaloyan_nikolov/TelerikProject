import {createSlice} from "@reduxjs/toolkit";

export const filterTags = createSlice({
    name: "filterTags",
    initialState: {id: []},
    reducers: {
        setTagsId(state, actions) {
            if (!state.id.includes(actions.payload)) {
                state.id = [...state.id, actions.payload];
            }
        },
        removeTagsId(state, actions) {
            state.id = state.id.filter(current => current !== actions.payload);
        },
        resetTagsIds(state) {
            state.id = [];
        }
    }
});

export const {setTagsId, removeTagsId,resetTagsIds} = filterTags.actions;

export default filterTags.reducer;
