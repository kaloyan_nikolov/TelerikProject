import {createSlice} from "@reduxjs/toolkit";

export const filterManufacturer = createSlice({
    name: "filterManufacturer",
    initialState: {id: []},
    reducers: {
        setManufacturerId(state, actions) {
            if (!state.id.includes(actions.payload)) {
                state.id = [...state.id, actions.payload];
            }
        },
        removeManufacturerId(state, actions) {
            state.id = state.id.filter(current => current !== actions.payload);
        },
        resetManufacturerIds(state) {
            state.id = [];
        }
    }
});

export const {setManufacturerId, removeManufacturerId,resetManufacturerIds} = filterManufacturer.actions;

export default filterManufacturer.reducer;
