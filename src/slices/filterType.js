import {createSlice} from "@reduxjs/toolkit";

export const filterType = createSlice({
    name: "filterType",
    initialState: {id: []},
    reducers: {
        setMaterialTypeId(state, actions) {
            if (!state.id.includes(actions.payload)) {
                state.id = [...state.id, actions.payload];
            }
        },
        removeMaterialTypeId(state, actions) {
            state.id = state.id.filter(current => current !== actions.payload);
        },
        resetMaterialTypeIds(state) {
            state.id = [];
        }
    }
});

export const {setMaterialTypeId, removeMaterialTypeId,resetMaterialTypeIds} = filterType.actions;

export default filterType.reducer;
