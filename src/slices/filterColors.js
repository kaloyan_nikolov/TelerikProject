import {createSlice} from "@reduxjs/toolkit";

export const filterColors = createSlice({
    name: "filterColors",
    initialState: {id: []},
    reducers: {
        setColorsId(state, actions) {
            if (!state.id.includes(actions.payload)) {
                state.id = [...state.id, actions.payload];
            }
        },
        removeColorsId(state, actions) {
            state.id = state.id.filter(current => current !== actions.payload);
        },
        resetColorsIds(state) {
            state.id = [];
        }
    }
});

export const {setColorsId, removeColorsId,resetColorsIds} = filterColors.actions;

export default filterColors.reducer;
