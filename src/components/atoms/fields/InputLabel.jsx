import React from "react";
import PropTypes from "prop-types";

const InputLabel = (props) => {
  return (
    <input
      id={props.id}
      type={props.type}
      ref={props.ref}
      autoComplete={props.autoComplete}
      onChange={props.onChangeFunc}
      value={props.value}
      required={props.InputRequired}
    />
  );
};

InputLabel.defaultProps = {
  autoComplete: "off",
  InputRequired: false,
};

InputLabel.propTypes = {
  type: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
  ref: PropTypes.node,
  autoComplete: PropTypes.string,
  onChangeFunc: PropTypes.func,
  value: PropTypes.string,
  InputRequired: PropTypes.bool,
};

export default InputLabel;
