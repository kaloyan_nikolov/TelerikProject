import React from "react";
import classes from './CheckboxColor.module.css'
import {useDispatch, useSelector} from "react-redux";
import {removeColorsId, setColorsId} from "../../../slices/filterColors";
import {removeIndustriesId, setIndustriesId} from "../../../slices/filterIndustries";
import {removeManufacturerId, setManufacturerId} from "../../../slices/filterManufacturer";
import {removeMaterialTypeId, setMaterialTypeId} from "../../../slices/filterType";
import {removeTagsId, setTagsId} from "../../../slices/filterTags";

const Checkbox = ({title, backGroundColor, ids, idStoreKey, typeCheckbox}) => {

    const dispatch = useDispatch()
    const checkIds = useSelector(state => state[idStoreKey].id)
    const checked = checkIds.includes(`${ids}`)

    const handleChange = (e) => {

        switch (typeCheckbox) {
            case "color" :
                checked ? dispatch(removeColorsId(e.target.id)) : dispatch(setColorsId(e.target.id))
                break
            case "industries" :
                checked ? dispatch(removeIndustriesId(e.target.id)) : dispatch(setIndustriesId(e.target.id))
                break
            case "manufacturer" :
                checked ? dispatch(removeManufacturerId(e.target.id)) : dispatch(setManufacturerId(e.target.id))
                break
            case "material" :
                checked ? dispatch(removeMaterialTypeId(e.target.id)) : dispatch(setMaterialTypeId(e.target.id))
                break
            case "tag" :
                checked ? dispatch(removeTagsId(e.target.id)) : dispatch(setTagsId(e.target.id))
                break
            default:
        }
    }

    return (
        <>
            <label className={typeCheckbox === "color" ? classes.container : ""}>
                <input type="checkbox" checked={checked} id={ids} onChange={handleChange}/>
                <span className={classes.titleLabel}>
                    {title}
                </span>

                {typeCheckbox === "color" ? <span className={classes.checkmark} style={{backgroundColor: backGroundColor}}></span> : null}
            </label>
        </>
    )
}
export default Checkbox;
