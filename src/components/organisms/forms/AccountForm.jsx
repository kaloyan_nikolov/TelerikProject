import React, { useState, useEffect } from "react";
import InputLabel from "../../atoms/fields/InputLabel";
import Avatar from "../users/Avatar";
import { supabase } from "../../../lib/supabaseClient";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";

const AccountForm = () => {
  const sessionAuth = useSelector((state) => state.userReducer);

  const [loading, setLoading] = useState(true);
  const [email, setEmail] = useState("");
  const [username, setUsername] = useState(null);
  const [firstName, setFirstName] = useState(null);
  const [lastName, setLastName] = useState(null);
  const [password, setPassword] = useState(null);
  const [avatar_url, setAvatarUrl] = useState(null);

  useEffect(() => {
    setEmail(sessionAuth?.Auth.user["email"]);
  }, []);

  useEffect(() => {
    getProfile();
  }, [sessionAuth]);

  const getProfile = async () => {
    try {
      setLoading(true);
      const { user } = sessionAuth.Auth;

      let { data, error, status } = await supabase
        .from("profiles")
        .select("*")
        .eq("id", user.id)
        .single();

      if (error && status !== 406) {
        throw error;
      }

      if (data) {
        setUsername(data.username);
        setFirstName(data.firstName);
        setLastName(data.lastName);
        setAvatarUrl(data.avatar_url);
      }
    } catch (error) {
      alert(error.message);
    } finally {
      setLoading(false);
    }
  };

  const updateProfile = async () => {
    try {
      setLoading(true);
      const { user } = sessionAuth.Auth;

      const updates = {
        id: user.id,
        username,
        firstName,
        lastName,
        avatar_url,
        updated_at: new Date(),
      };

      let { error } = await supabase.from("profiles").upsert(updates);

      if (error) {
        throw error;
      }
    } catch (error) {
      alert(error.message);
    } finally {
      setLoading(false);
    }
  };

  const updatePassword = async (e) => {
    e.preventDefault(e);

    try {
      setLoading(true);
      let { data, error } = supabase.auth.updateUser({ password: password });

      console.log(data);

      if (error) {
        throw error;
      }
    } catch (error) {
      alert(error.message);
    } finally {
      alert("Password changed successfully!");
      setPassword(null);
      setLoading(false);
    }
  };

  return (
    <>
      <div aria-live="polite">
        {loading ? (
          "Saving ..."
        ) : (
          <form onSubmit={updateProfile} className="form-widget">
            <Avatar
              url={avatar_url}
              size={150}
              onUpload={(url) => {
                setAvatarUrl(url);
                updateProfile({ username, avatar_url: url });
              }}
            />
            <div>Email: {email}</div>
            <div>
              <label htmlFor="username">Username</label>
              <InputLabel
                id="username"
                type="text"
                value={username || ""}
                onChangeFunc={(e) => setUsername(e.target.value)}
              />
            </div>
            <div>
              <label htmlFor="firstName">First name</label>
              <InputLabel
                id="firstName"
                type="text"
                value={firstName || ""}
                onChangeFunc={(e) => setFirstName(e.target.value)}
              />
            </div>
            <div>
              <label htmlFor="lastName">Last name</label>
              <InputLabel
                id="lastName"
                type="text"
                value={lastName || ""}
                onChangeFunc={(e) => setLastName(e.target.value)}
              />
            </div>
            <div>
              <button className="button primary block" disabled={loading}>
                Update profile
              </button>
            </div>
          </form>
        )}

        <form onSubmit={updatePassword} className="form-widget">
          <div>
            <label htmlFor="password">Change password</label>
            <InputLabel
              id="password"
              type="password"
              value={password || ""}
              onChangeFunc={(e) => setPassword(e.target.value)}
            />
          </div>
          <div>
            <button className="button primary block" disabled={loading}>
              Update password
            </button>
          </div>
        </form>
        <Link to="/home">Back to home</Link>
      </div>
    </>
  );
};

export default AccountForm;
