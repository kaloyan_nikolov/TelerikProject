import React, { useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { setSession } from "../../../api/logging/sessionSlice";
import { supabase } from "../../../lib/supabaseClient";
import styles from "../../organisms/forms/LoginForm.module.css";
import InputLabel from "../../atoms/fields/InputLabel";

const LoginForm = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const [email, setEmail] = useState("");
  const [pwd, setPwd] = useState("");

  const handleSubmit = async (e) => {
    e.preventDefault();
    const { data, error } = await supabase.auth.signInWithPassword({
      email: email,
      password: pwd,
    });

    const auth = data.user?.aud;

    if (auth === "authenticated") {
      dispatch(setSession(data));
      navigate("/");
    } else if (error) {
      alert(error);
    }
  };

  return (
    <>
      <h1 className={styles.title}>Sign In</h1>
      <form onSubmit={handleSubmit} className={styles.form}>
        <label>Email:</label>
        <InputLabel
          id="email"
          type="text"
          onChangeFunc={(e) => setEmail(e.target.value)}
          value={email}
          InputRequired={true}
        />
        <label>Password:</label>
        <InputLabel
          id="password"
          type="password"
          onChangeFunc={(e) => setPwd(e.target.value)}
          value={pwd}
          InputRequired={true}
        />
        <button id="loginButton">Login</button>
      </form>
      <p className={styles.login_registration_link}>
        Need an Account?
        <br />
        <span className="line">
          <Link id="regLink" to="/registration">Registration</Link>
        </span>
      </p>
    </>
  );
};

export default LoginForm;
