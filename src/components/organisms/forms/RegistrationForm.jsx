import React, { useEffect, useRef, useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import styles from "./RegistrationForm.module.css";
import { AiOutlineCheck } from "react-icons/ai";
import { FaTimes } from "react-icons/fa";
import { BsInfoCircle } from "react-icons/bs";

import { supabase } from "../../../lib/supabaseClient";

const RegistrationForm = () => {
  const USER_REGEX = /^[A-z][A-z0-9-_]{3,23}$/;
  const PWD_REGEX = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%]).{6,24}$/;
  const EMAIL_REGEX = /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i;

  const navigate = useNavigate();

  const userRef = useRef();
  const errRef = useRef();

  const [user, setUser] = useState("");
  const [validName, setValidName] = useState(false);
  const [userFocus, setUserFocus] = useState(false);

  const [pwd, setPwd] = useState("");
  const [validPwd, setValidPwd] = useState(false);
  const [pwdFocus, setPwdFocus] = useState(false);

  const [email, setEmail] = useState("");
  const [validEmail, setValidEmail] = useState(false);
  const [emailFocus, setEmailFocus] = useState(false);

  const [errMsg, setErrMsg] = useState("");
  // const [success, setSuccess] = useState(false);

  async function handleSubmit(e) {
    e.preventDefault();

    const { data, error } = await supabase.auth.signUp({
      email: email,
      password: pwd,
      options: {
        data: {
          username: user,
          role: "user",
        },
      },
    });

    if (data) {
      navigate("/");
    } else if (error) {
      throw error;
    }
  }

  useEffect(() => {
    userRef.current.focus();
  }, []);

  useEffect(() => {
    setValidName(USER_REGEX.test(user));
    setValidPwd(PWD_REGEX.test(pwd));
    setValidEmail(EMAIL_REGEX.test(email));
  }, [pwd, email, user]);

  useEffect(() => {
    setErrMsg("");
  }, [user, pwd, email]);

  return (
    <section className={styles.box}>
      <div className={styles.formSection}>
        <p
          ref={errRef}
          className={errMsg ? styles.err_msg : styles.offscreen}
          aria-live="assertive"
        >
          {errMsg}
        </p>
        <h1 className={styles.title}>Register</h1>
        <form onSubmit={handleSubmit}>
          <label htmlFor="userName">
            Full Name:
            <AiOutlineCheck
              className={validName ? styles.valid : styles.hide}
            />
            <FaTimes
              className={validName || !user ? styles.hide : styles.invalid}
            />
          </label>
          <input
            type="text"
            id="userName"
            ref={userRef}
            // autoComplete="off"
            onChange={(e) => setUser(e.target.value)}
            value={user}
            required
            aria-invalid={validName ? "false" : "true"}
            aria-describedby="uidnote"
            onFocus={() => setUserFocus(true)}
            onBlur={() => setUserFocus(false)}
          />
          <p
            id="uidnote"
            className={
              userFocus && user && !validName
                ? styles.instructions
                : styles.offscreen
            }
          >
            <BsInfoCircle />
            4 to 24 characters.
            <br />
            Must begin with a letter.
            <br />
            Letters, numbers, underscores, hyphens allowed.
          </p>

          <label htmlFor="password">
            Password:
            <AiOutlineCheck className={validPwd ? styles.valid : styles.hide} />
            <FaTimes
              className={validPwd || !pwd ? styles.hide : styles.invalid}
            />
          </label>
          <input
            type="password"
            id="password"
            onChange={(e) => setPwd(e.target.value)}
            value={pwd}
            required
            aria-invalid={validPwd ? "false" : "true"}
            aria-describedby="pwdnote"
            onFocus={() => setPwdFocus(true)}
            onBlur={() => setPwdFocus(false)}
          />
          <p
            id="pwdnote"
            className={
              pwdFocus && !validPwd ? styles.instructions : styles.offscreen
            }
          >
            <BsInfoCircle />
            6 to 24 characters.
            <br />
            Must include uppercase and lowercase letters, a number and a special
            character.
            <br />
            Allowed special characters:
            <span aria-label="exclamation mark">!</span>
            <span aria-label="at symbol">@</span>
            <span aria-label="hashtag">#</span>
            <span aria-label="dollar sign">$</span>
            <span aria-label="percent">%</span>
          </p>

          <label htmlFor="email">
            Email:
            <AiOutlineCheck
              className={validEmail && email ? styles.valid : styles.hide}
            />
            <FaTimes
              className={validEmail || !email ? styles.hide : styles.invalid}
            />
          </label>
          <input
            type="email"
            id="email"
            onChange={(e) => setEmail(e.target.value)}
            value={email}
            required
            aria-invalid={validEmail ? "false" : "true"}
            aria-describedby="confirmnote"
            onFocus={() => setEmailFocus(true)}
            onBlur={() => setEmailFocus(false)}
          />
          <p
            id="confirmnote"
            className={
              emailFocus && !validEmail ? styles.instructions : styles.offscreen
            }
          >
            <BsInfoCircle />
            Please enter a valid email
          </p>

          {/* <button disabled={!validName || !validPwd || !validEmail}> */}
          <button id="registration">Registration</button>
        </form>
        <p className={styles.login_registration_link}>
          Already registered?
          <br />
          <span className="line">
            <Link to="/login">Login</Link>
          </span>
        </p>
      </div>
    </section>
  );
};

export default RegistrationForm;
