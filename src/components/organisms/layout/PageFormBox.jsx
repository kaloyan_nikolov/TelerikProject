import React from "react";
import PropTypes from "prop-types";
import LoginForm from "../forms/LoginForm";
import RegistrationForm from "../forms/RegistrationForm";
import AccountForm from "../forms/AccountForm";
import styles from "./PageFormBox.module.css";

const PageFormBox = ({ type }) => {
  function typeForm() {
    if (type === "login") {
      return <LoginForm />;
    } else if (type === "registration") {
      return <RegistrationForm />;
    } else if (type === "account") {
      return <AccountForm />;
    } else {
      throw new Error("Wrong access to forms for the user!");
    }
  }

  return (
    <section className={styles.box}>
      <div className={styles.formSection}>{typeForm()}</div>
    </section>
  );
};

PageFormBox.propTypes = {
  type: PropTypes.string.isRequired,
};

export default PageFormBox;
