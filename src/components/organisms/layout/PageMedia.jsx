import React from 'react';
import classes from "./PageMedia.module.css";

const PageMedia = ({children}) => {
    return (
        <section className={classes.mainContainer}>
            {children}
        </section>
    );
};

export default PageMedia;