import React from 'react';
import classes from "./PageMain.module.css";

const PageMain = ({children}) => {
    return (
        <section className={classes.mainContainer}>
            {children}
        </section>
    );
};

export default PageMain;