import React from 'react';
import classes from './MaterialTypeFilter.module.css'
import {useGetAllMaterialsQuery} from "../../../api/filtersApi/filterMaterialsApi";
import {filterType} from "../../../slices/filterType";
import Checkbox from "../../atoms/fields/Checkbox";

const MaterialTypeFilter = () => {

    const {data: allMaterials, isFetching} = useGetAllMaterialsQuery()

    return (
        <div className={classes.checkBoxes}>
            <div className={classes.title}> MATERIAL TYPE</div>
            {!isFetching ? allMaterials.map((material, index) =>
                <Checkbox
                    key={index}
                    title={material.name}
                    ids={material.id}
                    idStoreKey={"filterType"}
                    typeCheckbox={material.checkbox_type}
                />
            ) : <div>Skeleton Loading</div>
            }
            <hr style={{margin: '2rem'}}/>
        </div>
    );
};

export default MaterialTypeFilter;
