import React from "react";
import classes from "./MaterialColorFilter.module.css";
import {useGetAllColorQuery} from "../../../api/filtersApi/filterColorsApi";
import Checkbox from "../../atoms/fields/Checkbox";


const MaterialColorFilter = () => {
    const {data: allColors, isFetching} = useGetAllColorQuery()

    return (
        <div className={classes.background}>
            <div className={classes.title}>MATERIAL COLOR</div>
            <div className={classes.checkBoxes}>
                {!isFetching ? allColors.map((color, index) =>
                    <Checkbox
                        key={index}
                        backGroundColor={color.hex}
                        ids={color.id}
                        idStoreKey={"filterColors"}
                        typeCheckbox={color.checkbox_type}/>
                ) : <div>Skeleton Loading</div>
                }
            </div>
            <hr style={{margin: '2rem'}}/>
        </div>
    );
};

export default MaterialColorFilter;
