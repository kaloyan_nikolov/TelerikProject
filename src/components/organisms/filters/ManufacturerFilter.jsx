import React from 'react';
import classes from './MaterialTypeFilter.module.css'
import {useGetAllManufacturerQuery} from "../../../api/filtersApi/filterManufacturerApi";
import Checkbox from "../../atoms/fields/Checkbox";


const ManufacturerFilter = () => {

    const {data: allManufacturer, isFetching} = useGetAllManufacturerQuery()

    return (
        <div className={classes.checkBoxes}>
            <div className={classes.title}>MANUFACTURER</div>
            {!isFetching ? allManufacturer.map((manufacturer, index) =>
                <Checkbox
                    key={index}
                    title={manufacturer.name}
                    ids={manufacturer.id}
                    idStoreKey={"filterManufacturer"}
                    typeCheckbox={manufacturer.checkbox_type}/>
            ) : <div>Skeleton Loading</div>
            }
            <hr style={{margin: '2rem'}}/>
        </div>
    );
};

export default ManufacturerFilter;