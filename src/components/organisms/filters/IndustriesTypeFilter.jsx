import React from 'react';
import classes from './MaterialTypeFilter.module.css'
import {useGetAllIndustriesQuery} from "../../../api/filtersApi/filterIndustriesApi";
import Checkbox from "../../atoms/fields/Checkbox";

const IndustriesTypeFilter = () => {
  const { data: allIndustries, isFetching } = useGetAllIndustriesQuery();

    return (
        <div className={classes.checkBoxes}>
            <div className={classes.title}>INDUSTRIES TYPE</div>
            {!isFetching ? allIndustries.map((industries, index) =>
                <Checkbox
                    key={index}
                    title={industries.name}
                    ids={industries.id}
                    idStoreKey={"filterIndustries"}
                    typeCheckbox={industries.checkbox_type}
                />
            ) : <div>Skeleton Loading</div>
            }
            <hr style={{margin: '2rem'}}/>
        </div>
    );
};

export default IndustriesTypeFilter;
