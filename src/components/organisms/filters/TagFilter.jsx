import React from 'react';
import classes from './MaterialTypeFilter.module.css'
import {useGetAllTagsQuery} from "../../../api/filtersApi/filterTagsApi";
import {filterTags} from "../../../slices/filterTags";
import Checkbox from "../../atoms/fields/Checkbox";

const TagFilter = () => {

    const {data: allTags, isFetching} = useGetAllTagsQuery()

    return (
        <div className={classes.checkBoxes}>
            <div className={classes.title}>TAG</div>
            {!isFetching ? allTags.map((tag, index) =>
                <Checkbox
                    key={index}
                    title={tag.name}
                    ids={tag.id}
                    idStoreKey={"filterTags"}
                    typeCheckbox={tag.checkbox_type}
                />
            ) : <div>Skeleton Loading</div>
            }
            <hr style={{margin: '2rem'}}/>
        </div>
    );
};

export default TagFilter;
