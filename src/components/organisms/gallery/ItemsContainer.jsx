import React, {useEffect, useState} from "react";
import ItemCard from "../../molecules/ItemCard";
import classes from "./ItemsContainer.module.css";
import {Waypoint} from "react-waypoint";
import {useDispatch, useSelector} from "react-redux";
import {supabase} from "../../../lib/supabaseClient";
import {resetMaterialTypeIds} from "../../../slices/filterType";
import {resetColorsIds} from "../../../slices/filterColors";
import {resetManufacturerIds} from "../../../slices/filterManufacturer";
import {resetTagsIds} from "../../../slices/filterTags";
import {resetIndustriesIds} from "../../../slices/filterIndustries";

const ItemsContainer = () => {

    const dispatch = useDispatch()

    const [value, setValue] = useState("")
    const [search, setSearch] = useState("")
    const [data, setData] = useState([])
    const [pageRange, setPageRange] = useState({start: 0, end: 20, page: 0})

    const materialTypeIds = useSelector(state => state.filterType.id)
    const manufacturerId = useSelector(state => state.filterManufacturer.id)
    const industriesIds = useSelector(state => state.filterIndustries.id)
    const colorsIds = useSelector(state => state.filterColors.id)
    const tagsIds = useSelector(state => state.filterTags.id)

    useEffect(() => {
        fetchData().then((newData) => {
            setData((prevData) => [...prevData, ...newData]);
        });
    }, [pageRange.page]);

    useEffect(() => {
        fetchData().then((newData) => {
            setData([...newData]);
            setPageRange({start: 0, end: 20, page: 0});
        });
    }, [materialTypeIds, manufacturerId, industriesIds, colorsIds, tagsIds, search,]);

    async function fetchData() {
        if (
            search &&
            (materialTypeIds.length > 0 ||
                manufacturerId.length > 0 ||
                industriesIds.length > 0 ||
                colorsIds.length > 0 ||
                tagsIds.length > 0)
        ) {
            let {data} = await supabase
                .from("vrscans")
                .select("*")
                .or(`materialTypeId.cs.{${materialTypeIds}}`)
                .or(`manufacturerId.cs.{${manufacturerId}}`)
                .or(`industries.cs.{${industriesIds}}`)
                .or(`colors.cs.{${colorsIds}}`)
                .or(`tags.cs.{${tagsIds}}`)
                .ilike("name", `%${search}%`)
                .order("id", {ascending: true})
                .range(pageRange.start, pageRange.end);
            return data;
        } else if (
            materialTypeIds.length > 0 ||
            manufacturerId.length > 0 ||
            industriesIds.length > 0 ||
            colorsIds.length > 0 ||
            tagsIds.length > 0
        ) {
            let {data} = await supabase
                .from("vrscans")
                .select("*")
                .or(`materialTypeId.cs.{${materialTypeIds}}`)
                .or(`manufacturerId.cs.{${manufacturerId}}`)
                .or(`industries.cs.{${industriesIds}}`)
                .or(`colors.cs.{${colorsIds}}`)
                .or(`tags.cs.{${tagsIds}}`)
                .order("id", {ascending: true})
                .range(pageRange.start, pageRange.end);
            return data;
        } else if (search) {
            let {data} = await supabase
                .from("vrscans")
                .select("*")
                .ilike("name", `%${search}%`)
                .order("id", {ascending: true})
                .range(pageRange.start, pageRange.end);
            return data;
        } else {
            let {data} = await supabase
                .from("vrscans")
                .select("*")
                .order("id", {ascending: true})
                .range(pageRange.start, pageRange.end);
            return data;
        }
    }

    const handleSearch = async (e) => {
        e.preventDefault();
        setSearch(value);
    };

    const handleReset = () => {
        dispatch(resetMaterialTypeIds());
        dispatch(resetColorsIds());
        dispatch(resetManufacturerIds());
        dispatch(resetTagsIds());
        dispatch(resetIndustriesIds());
        setSearch("");
        setValue("");
        setPageRange({start: 0, end: 20, page: 0});
        fetchData();
    };

    return (
        <div className={classes.container}>
            <div className={classes.searchBar}>
                <form onSubmit={handleSearch}>
                    <input
                        type="text"
                        placeholder="search"
                        value={value}
                        onChange={(event) => setValue(event.target.value)}
                    />
                    <button id="searchButton" type="submit">Search</button>
                    <button id="resetButton" type="button" onClick={handleReset}>
                        Reset
                    </button>
                </form>
            </div>

            <div id="itemsContainer" className={classes.itemsContainer}>
                {data.map((allItems, index) => (
                    <div className={classes.bodyCard} key={index}>
                        {data.length - 1 === index ?
                            (
                                <Waypoint
                                    key={index}
                                    onEnter={() => {
                                        setPageRange((prevState) => ({
                                            start: prevState.end,
                                            end: prevState.end + 10,
                                            page: prevState.page + 1,
                                        }));
                                    }}>
                                    <div>
                                        <ItemCard
                                            key={index}
                                            title={allItems.name}
                                            id={allItems.id}
                                            thumb={allItems.thumb}
                                            fileName={allItems.fileName}
                                        />
                                    </div>
                                </Waypoint>
                            )
                            :
                            (
                                <ItemCard
                                    key={index}
                                    title={allItems.name}
                                    id={allItems.id}
                                    thumb={allItems.thumb}
                                    fileName={allItems.fileName}
                                />
                            )
                        }
                    </div>
                ))}
            </div>
            {/*{data.length === 0 && <p className={classes.noResults}> There is No Results!!!</p>}*/}
        </div>
    );
};

export default ItemsContainer;