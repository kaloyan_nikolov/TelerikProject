import React, {useEffect, useState} from "react";
import Slide from "../atoms/carosel/Slide";
import SliderControls from "../atoms/carosel/SliderControls";
import SliderNav from "../atoms/carosel/SliderNav";
import {slides} from "../atoms/carosel/slidesData";
import classes from "./Carosel.module.css"


const Carousel = ({
  initialIndex = 0,
  infiniteLoop = true,
  autoplay = true,
  autoplayInterval = 3000
  }) => {
    const [slideIndex, setSlideIndex] = useState(initialIndex)

    useEffect(() => {
        if (autoplay) {
            const interval = setInterval(() => {
                setSlideIndex((current) =>
                    current === slides.length - 1 ? 0 : current + 1
                )
            }, autoplayInterval)
            return () => clearInterval(interval)
        }
    }, [])

    return (
        <section className={classes.container}>
            <Slide
                slides={slides}
                slideIndex={slideIndex}
                tittle={slides}
            />
            <SliderControls
                slides={slides}
                slideIndex={slideIndex}
                setSlideIndex={setSlideIndex}
                infiniteLoop={infiniteLoop}
            />
            <SliderNav
                slides={slides}
                slideIndex={slideIndex}
                setSlideIndex={setSlideIndex}
            />
        </section>
    )
}

export default Carousel;