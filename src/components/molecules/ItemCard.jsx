import React from 'react';
import classes from "./ItemCard.module.css"


const ItemCard = ({title, id, thumb, fileName}) => {

    return (
        <div className={classes.itemBody}>
            <div className={classes.itemContent}>
                <img src={thumb} alt="vrScan image" className={classes.itemImage}/>
                <h5>{title}</h5>
                <h3 className={classes.name}>{fileName}</h3>
            </div>
        </div>
    );
};

export default ItemCard;