import React from "react";
import MaterialTypeFilter from "../../organisms/filters/MaterialTypeFilter";
import MaterialColorFilter from "../../organisms/filters/MaterialColorFilter";
import TagFilter from "../../organisms/filters/TagFilter";
import IndustriesTypeFilter from "../../organisms/filters/IndustriesTypeFilter";
import ManufacturerFilter from "../../organisms/filters/ManufacturerFilter";

const Aside = () => {
    return (
        <div style={{display:"flex",flexDirection:"column",gap:'50px', width:"20%" , marginLeft:"10rem" }}  >
            <MaterialTypeFilter/>
            <MaterialColorFilter/>
            <TagFilter/>
            <IndustriesTypeFilter/>
            <ManufacturerFilter/>
        </div>
    );
};

export default Aside;
