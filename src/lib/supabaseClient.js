/* eslint-disable no-undef */
import { createClient } from "@supabase/supabase-js";

const supabaseUrl = process.env.REACT_APP_SUPABASE_URL;
const supabaseKey = process.env.REACT_APP_SUPABASE_KEY;

const options = {
    db: true,
    auth: true,
    realtime: true,
};

export const supabase = createClient(
    supabaseUrl,
    supabaseKey,
    options
);